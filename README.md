# fccloud



## Getting started

```sh
export no_proxy="artemis,10.0.213.11,trial2.rss.rancher.hpc"
export PREFECT_API_URL=http://10.0.213.11:30003/api
```

Create venv and install packages

```sh
# work this out
```

Create deployment

```
prefect deploy
```

Modify the work_pool image name:

```yaml
deployments:
- name: Test flow
  version: null
  tags: []
  description: Example flow
  entrypoint: flows/test_flow.py:env_info
  parameters: {}
  work_pool:
    name: asdi-k8s
    work_queue_name: null
    job_variables: 
      image: demeter:5050/qld/des/rsc/fcv3cloud:dev
  schedule: null
```
Push your changes, and run. You need a worker running somewhere,

```sh

prefect worker start --pool 'asdi-k8s'
```



```sh
prefect deployment run 'Example Flow/Test flow'
```
'Example Flow' comes from your code, it's the name of your flow, but the 
'Test flow' is the name from your `prefect.yaml` file. If there are
no conflict I think you can drop the flowname (Example Flow).

You need to be in an  environment that allows you to run the code 
before you can deploy.

```sh
prefect deployment run 'fcv3_single_date/fcv3' --params '{"satids": ["S2B_55KEP_20230224_0_L2A", "S2B_55KFP_20230224_1_L2A"]}'
```

As you add more deployments, you build up a number of commands.

Here are some other examples:

```sh

prefect deployment run 'season run/seasonrun' --params '{"tile":"t55jgm", "endmonth": "202305","startmonth": "202303"}'

prefect deployment run 'do median/median_tile' --params '{"tile":"t55jgm", "endmonth": "202305","startmonth": "202303"}'

```




## Worker on kube node

It might be convenient to build an image to run your worker. For example,

```dockerfile
FROM prefecthq/prefect:2-python3.9

# Set the environment variables
ENV PREFECT_API_URL=http://10.0.213.11:30003/api
ENV no_proxy="artemis,10.0.213.11,trial2.rss.rancher.hpc"

RUN pip install prefect-kubernetes

# Run the worker command in the background
CMD ["prefect", "worker", "start", "--pool", "asdi-k8s"]

```

If you build, that, you can do:

```sh
docker run -d -e no_proxy="artemis,10.0.213.11,trial2.rss.rancher.hpc" \
  -v ~/.kube/config:/root/.kube/config \
  -e KUBECONFIG=/root/.kube/config \
  jrsrp/prefect_worker:latest
```

You can run your worker anywhere that has access kubernetes, but for some
reason I couldn't seem to get access from `prefect.hpc`, and I also couldn't
work out how to have a node in kubernetes for the worker. This might need 
some input from our DevOps engineer.

