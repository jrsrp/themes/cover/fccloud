from calendar import monthrange
from datetime import datetime, timedelta
import json
from pystac_client import Client

STAC_SERVER= "http://espb4stac.dnr.qld.gov.au/api/v1/pgstac/"


def yearmon_to_daterange(yearmon):
    """convert a yearmon string to a date range

    Args:
        yearmon (str): yearmon string like 201903

    Returns:
        _type_: _description_
    """
    startdate = datetime.strptime(f'{yearmon}01', '%Y%m%d')
    daysinmonth = monthrange(startdate.year, startdate.month)[1]
    enddate = startdate + timedelta(days=(daysinmonth)) - timedelta(minutes=1)
    return (startdate, enddate)


def get_geom_epsg(tile, jsonfile="flows/qld_tile_geoms.json"):
    # epsg from the standard tiles
    # but typically will be based on tile name
    # we use the same strategy as in generteSen2Aw1.py to get the epsg
    # and the geom
    # this used to be a query to our database, but to avoid
    # unnecessary network setup, I've dumped this data to file
    with open(jsonfile, "r") as json_file:
        data = json.load(json_file)
    utmzone = int(tile[1:3])
    geom = data[tile]['geom']
    epsgcode = data[tile]['epsgcode']
    return (geom, epsgcode)


def get_items(tile, geom, startdate, enddate):
    """
    get items matching tile and geom
    """
    filt = {'op': '=', 'args': [{'property': 'grid:code'}, f'MGRS-{tile.upper()[1:]}']}
    params = {
        'max_items': 200,
        'collections': 'sentinel2_sdt_asdi',
        'filter':filt,
        'intersects': geom,
        'datetime': (startdate, enddate)
    }
    catalog = Client.open(STAC_SERVER)
    results = catalog.search(**params)
    items = results.item_collection()
    return items