# first get the inputs
from utils import yearmon_to_daterange
from pystac_client import Client
import boto3
from prefect.blocks.system import Secret
from moamosaic.mosaic import doMosaic
import qvf_common as qvf
from prefect import flow, get_run_logger, task
from osgeo import gdal
import os


def generate_presigned_urls(session, s3_keys, expiration=3600):
    """
    Generate pre-signed URLs for a list of S3 keys using a provided boto3 session.

    :param session: A boto3.session.Session object
    :param s3_keys: A list of S3 URI strings
    :param expiration: Time in seconds for the pre-signed URL to remain valid
    :return: A list of pre-signed URLs
    """
    # Create S3 client using the provided session
    s3_client = session.client('s3')

    # Initialize the list to hold pre-signed URLs
    presigned_urls = []

    # Generate a pre-signed URL for each S3 key
    for s3_key in s3_keys:
        # Parse the S3 URI
        bucket_name = s3_key.split('/')[2]
        key_name = '/'.join(s3_key.split('/')[3:])

        try:
            # Generate the pre-signed URL
            presigned_url = s3_client.generate_presigned_url('get_object',
                                                            Params={'Bucket': bucket_name, 'Key': key_name},
                                                            ExpiresIn=expiration)
            presigned_urls.append(presigned_url)
        except Exception as e:
            print(f"Error generating pre-signed URL for {s3_key}: {e}")
            presigned_urls.append(None)

    return presigned_urls


def get_mos_name(tilename):
    fname = qvf.changelocation(tilename, 'qld')
    fname  = qvf.changelocation(tilename, 'qld')
    return fname

@task(name="validate inputs")
def validate_inputs(urls):
    logger = get_run_logger()
    validurls = []
    for url in urls:
        try:
            fh = gdal.Open(url)
            band = fh.GetRasterBand(1)
            validurls.append(url)
            logger.info(f"{url} is valid")
        except Exception as e:
            logger.info(f"failed to oepn {url}")
    return validurls



@task(name="mosaic component")
def mosaic_tiles(uris, localfname):
    logger = get_run_logger()
    logger.info("begin mosaic")
    logger.info(f"{localfname=}")
    doMosaic(
        uris,
        localfname,
        outprojepsg=3577,
        resamplemethod='cubic'
    )



@flow(name="qld median mosaic")
def create_qld_mosaics(startmonth, endmonth, product):
    """
    product in pv, npv, bare
    """

    logger = get_run_logger()
    logger.info("running median")

    startdate, _ = yearmon_to_daterange(startmonth)
    _, enddate = yearmon_to_daterange(endmonth)

    STAC_SERVER= "http://espb4stac.dnr.qld.gov.au/api/v1/pgstac/"

    params = {
        'max_items': 1000,
        'collections': 'sentinel2_sct_asdi',
        'datetime': (startdate, enddate)
    }
    catalog = Client.open(STAC_SERVER)
    results = catalog.search(**params)
    items = results.item_collection()

    # get the assets

    assets = []

    for item in items:
        assets.append(item.assets[product].href)

    aws_secret = Secret.load("imgreposecret")
    aws_key = Secret.load("imgrepokey")

    session = boto3.Session(
        aws_access_key_id=aws_key.get(), aws_secret_access_key=aws_secret.get()
    )
    s3 = session.client("s3")
    rss_bucket = "esp-rss-imagerepo"
    # generate urls

    uris  = generate_presigned_urls(session, assets)

    # each ofthese are mosaiced and saved
    # asset looks like localfname.split('/')[-1]
    # 's3://esp-rss-imagerepo/sentinel2/t56k/t56kmv/m202312202402/cfmsre_qld_m202312202402_aj9m6_bnpv.tif'
    # we want the base name
    valid_uris = validate_inputs(uris)
    basename = get_mos_name(assets[0].split('/')[-1])

    logger.info(f"found {len(valid_uris)} images to mosaic")
    mosaic_tiles(valid_uris, basename)
    key = f"sentinel2/qld/{basename}"
    s3.upload_file(basename, rss_bucket, str(key))
    os.unlink(basename)

