"""
Functions for manipulating QVF standard filenames. For info on QVF filenames,
see http://atrax/ciss/dres/qvf and also SLATS documentation.
"""

# These were all originally implemented as shell scripts, so this is my 'port' of the
# same set of utilities to python. However, I have tried to convert the flavour
# as well as the syntax, so hopefully they don't look too shell-like.
# Neil Flood. Sep 2004

# Stuff used locally
import os
from fnmatch import fnmatch
import datetime
import calendar

# For use with the satType parameter in the qvladmrawsubdir function.
RAWTYPE_USGSLANDSAT = "RAWTYPE_USGSLANDSAT"
RAWTYPE_SPOT = "RAWTYPE_SPOT"
RAWTYPE_ADS = "RAWTYPE_ADS"
RAWTYPE_MODIS = "RAWTYPE_MODIS"
RAWTYPE_ZY3 = "RAWTYPE_ZY3"
RAWTYPE_SENTINEL2 = "RAWTYPE_SENTINEL2"
RAWTYPE_SENTINEL1 = "RAWTYPE_SENTINEL1"

MOSAICNAMES_STATES = set(['qld', 'vic', 'nsw', 'nt', 'wa', 'sa', 'tas', 'aus'])

'''
# this list generated using something like:
with conn.cursor() as cursor:
    cursor.execute("""
    SELECT
        s2.scene AS scene,
        aust.state as state
        FROM sentinel2_tiles as s2
        JOIN state_boundaries as aust
        ON ST_Intersects(s2.geom, aust.geom);
        """)
    res = cursor.fetchall()
qldtiles = []
for tile, state in res:
    if 'QLD' in state:
        qldtiles.append(tile)
qldtiles = list(set(qldtiles))
'''


QLD_SENTINEL_TILES = [
    't55jhn', 't55jbh', 't55kfs', 't55kep', 't56klu', 't54kva', 't55khp', 't54luh', 't54jyq', 't54jws',
    't54kwu', 't54kug', 't55jbl', 't54kxc', 't54lxj', 't54kuc', 't55jgh', 't55jdl', 't55kcu', 't54lyk',
    't55kba', 't56jnr', 't56jnt', 't53kqq', 't54ktc', 't55kbv', 't54jxp', 't54lym', 't54kve', 't55kbq',
    't55jch', 't54jvq', 't54jzt', 't54lzj', 't56jns', 't54jys', 't54kvd', 't55kdv', 't56kkv', 't56jlt',
    't54lxh', 't55jel', 't55khr', 't54ktd', 't54lyp', 't54lwj', 't54jwn', 't54kuf', 't55jgn', 't56jkq',
    't56jmt', 't54lyr', 't54jvs', 't54ktf', 't54kwg', 't54kvf', 't54lzh', 't55lbe', 't56kkb', 't54lxl',
    't54lyj', 't54lwp', 't55jcl', 't56jks', 't55kgs', 't54kvu', 't56jms', 't54lyn', 't54kxv', 't54kyf',
    't56jkp', 't54jus', 't54kxd', 't54kzu', 't55jck', 't55jfk', 't54lzn', 't55jbn', 't54lzq', 't56jmq',
    't54lwl', 't54ktg', 't55ket', 't53kqr', 't54jts', 't54jwr', 't54kyu', 't55jfh', 't54jut', 't54jxt',
    't54lzk', 't54jxr', 't55jgj', 't55kcr', 't54jvt', 't55jdn', 't54lxn', 't55lcc', 't55jeh', 't55jbj',
    't56jln', 't54lzp', 't54kza', 't54kzf', 't54lxk', 't55kfq', 't53jrm', 't54kzb', 't55jgl', 't54kwd',
    't56kmu', 't55lcd', 't56kku', 't54kxf', 't55keq', 't54kxg', 't54kye', 't55jbk', 't53krq', 't54jyr',
    't55kcp', 't55kdu', 't55lbc', 't54jwp', 't54kwv', 't54lxp', 't56jlr', 't55kgt', 't54kuv', 't54kze',
    't54kyc', 't55jdh', 't53jqn', 't54jxs', 't54jvn', 't55kdp', 't56jls', 't55jej', 't56kmv', 't55kfu',
    't55jdk', 't55kbb', 't55jfm', 't54kua', 't54ktu', 't55kcs', 't54kvb', 't54kzc', 't54kud', 't54kwe',
    't56jmp', 't53kra', 't54kxb', 't54kwc', 't55kca', 't55kgr', 't54lxm', 't54kta', 't55kcv', 't55jcj',
    't54jwt', 't54jxq', 't56jnp', 't54lyh', 't55jcm', 't54lwk', 't55lbd', 't54kue', 't53kru', 't55kes',
    't56jkt', 't55kdb', 't54kya', 't55kbt', 't54jvr', 't54kxu', 't54jyp', 't54kxa', 't54kwf', 't56jlp',
    't53krr', 't54kyd', 't55kbp', 't54jxn', 't54kyg', 't55ker', 't54lwm', 't54lxq', 't55keu', 't54jzs',
    't56jmr', 't53jrn', 't53krp', 't55jek', 't54kzg', 't55khq', 't53krt', 't55kfp', 't56jnq', 't54kvg',
    't55jgm', 't55khs', 't54lyq', 't54kxe', 't55kfr', 't55kgp', 't54lwq', 't55kbu', 't54lzl', 't54jyt',
    't55kdr', 't55kds', 't53kqp', 't55kgq', 't56klv', 't55jfn', 't55jbm', 't55jen', 't54kvv', 't54kub',
    't54jyn', 't55jdm', 't55kdq', 't55kbr', 't54jtt', 't54lwh', 't55jcn', 't54kzv', 't53jqm', 't55jfl',
    't54kyb', 't53krb', 't54jwq', 't55kcq', 't54lyl', 't53krs', 't55kft', 't54kwa', 't55jem', 't56kka',
    't55kct', 't54ktv', 't55kbs', 't54kyv', 't54ktb', 't55jdj', 't53krv', 't55kda', 't55jfj', 't55jhm',
    't54lwn', 't55kdt', 't54kzd', 't54kte', 't54kvc', 't54kuu', 't54jvp', 't56jlq', 't55kcb', 't55jgk',
    't56jkr', 't54kwb']




def components(fullpath):
    """
    Return basic components of a QVF filename. Removes any leading directory
    components and parts after first "." character, so we are dealing
    with just the base filename, and splits this about "_".
    """
    filename = os.path.basename(fullpath)
    # basename is before first "."
    basename = filename.split('.')[0]
    # Components are separated by "_"
    components = basename.split('_')
    if len(components) < 4:
        raise MissingComponentError("Not a valid QVF filename. Filename '%s' has too few components"%fullpath)
    return components


def isQVFfilename(fullpath):
    """
    Returns True if the given filename appears to be a compliant QVF filename.
    May add more tests here for later, currently just this simple one.

    """
    isQVF = True
    try:
        componentList = components(fullpath)
    except MissingComponentError:
        componentList = []
        isQVF = False

    # The first component must always have exactly 6 characters.
    if isQVF and len(componentList[0]) != 6:
        isQVF = False

    return isQVF


def satellite(filename):
    "Extract the satellite ID from a QVF filename"
    return components(filename)[0][0:2]


def instrument(filename):
    "Extract instrument ID from a QVF filename"
    return components(filename)[0][2:4]


def product(filename):
    "Extract product ID from a QVF filename"
    return components(filename)[0][4:6]


def locationid(filename):
    "Extract location ID from a QVF filename"
    return components(filename)[1]


def acqdate(filename, format="yyyymmdd", list=False):
    """
    Extract acquisition date from a QVF filename
        format={yyyymmdd|yyyymm|yyyy|mm|dd|mmdd|yy|yymmdd}
        list=True makes a list of date components, instead of a single string
    """
    when = components(filename)[2]
    flagChar = when[0]
    if flagChar in "0123456789n":
        if flagChar == 'n':
            when = when[1:]

        if format == "yyyymmdd":
            outList = [when[0:4], when[4:6], when[6:8]]
        elif format == "yyyymm":
            outList = [when[0:4], when[4:6]]
        elif format == "yyyy":
            outList = when[0:4]
        elif format == "mm":
            outList = when[4:6]
        elif format == "mmdd":
            outList = [when[4:6], when[6:8]]
        elif format == "dd":
            outList = when[6:8]
        elif format == "yy":
            outList = when[2:4]
        elif format == "yymmdd":
            outList = [when[2:4], when[4:6], when[6:8]]
        else:
            outList = []
    else:
        outList = []

    # If we have been asked for a list, just return it, otherwise join it together
    # as a single string
    if list:
        return outList
    else:
        return "".join(outList)


def when(filename):
    "Extract the 'when' component of a QVF filename, without further processing"
    return components(filename)[2]

def when_dateobj(filename):
    """
    Return a datetime.date() object for the date of the given filename.

    Copes with "nominal" dates (i.e. starting with "n").

    The argument can be either a whole filename, or just the "when"
    field of the filename.

    """
    try:
        datestr = when(filename)
    except MissingComponentError as e:
        if filename.find('_') == -1:
            # We have only one component, so assume it is the whenstr
            datestr = filename
        else:
            # This is some other unknown sort of problem, so just re-raise the exception
            raise e

    if datestr.startswith('n'):
        datestr = datestr[1:]
    return dateobjfromstr(datestr)


def dateobjfromstr(datestr):
    "Make a datetime.date() object from a yyyymmdd string"
    if len(datestr) != 8:
        raise InvalidWhenTypeError("Date string '%s' is not yyyymmdd format"%datestr)
    year = int(datestr[:4])
    month = int(datestr[4:6])
    day = int(datestr[6:])
    return datetime.date(year, month, day)

def when_daterange(filename):
    """
    For a date range, return a pair of datetime.date() objects for the start
    and end dates respectively (as a list of two elements).

    If used with a day range, will return the two days. If used with month or year
    ranges, will return date ranges for the earliest and latest possible dates
    which would fit within the range. If used with a single month (yyyymm) or
    single year (yyyy), will return the first and last dates of those.

    The argument can be either a whole filename, or just the "when" field of a filename.

    """
    filename = str(filename)
    try:
        whenstr = when(filename)
    except MissingComponentError as e:
        if filename.find('_') == -1:
            # We have only one component, so assume it is the whenstr
            whenstr = filename
        else:
            # This is some other unknown sort of problem, so just re-raise the exception
            raise e

    tagChar = whenstr[0]
    if tagChar == 'd':
        startdate = dateobjfromstr(whenstr[1:9])
        enddate = dateobjfromstr(whenstr[9:])
    elif tagChar == 'm':
        startYM = whenstr[1:7]
        endYM = whenstr[7:13]
        startdate = datetime.date(int(startYM[:4]), int(startYM[4:]), 1)
        endYear = int(endYM[:4])
        endMonth = int(endYM[4:])
        (weekDay, daysInMonth) = calendar.monthrange(endYear, endMonth)
        enddate = datetime.date(endYear, endMonth, daysInMonth)
    elif tagChar == 'y':
        startYear = int(whenstr[1:5])
        endYear = int(whenstr[5:])
        startdate = datetime.date(startYear, 1, 1)
        enddate = datetime.date(endYear, 12, 31)
    elif len(whenstr) == 6 and whenstr.isdigit():
        # Start and end of a single month
        year = int(whenstr[:4])
        month = int(whenstr[4:6])
        startdate = datetime.date(year, month, 1)
        (weekDay, daysInMonth) = calendar.monthrange(year, month)
        enddate = datetime.date(year, month, daysInMonth)
    elif len(whenstr) == 4 and whenstr.isdigit():
        # Start and end of a single year
        year = int(whenstr)
        startdate = datetime.date(year, 1, 1)
        enddate = datetime.date(year, 12, 31)
    else:
        raise InvalidWhenTypeError("'When' field '%s' is not a date range. "%whenstr +
            "Must start with 'd', 'm' or 'y', or be 'yyyymm' or 'yyyy' form")

    return [startdate, enddate]

def when_rangecentre(filename):
    """
    For a date range, return a dateobj corresponding to the centre of the range
    """
    (startdate, enddate) = when_daterange(filename)

    nDays = (enddate - startdate).days + 1
    dateCtr = startdate + datetime.timedelta(nDays // 2)
    return dateCtr


def standardSeason(startYear, startMonth):
    """
    Return a season specifier for the given standard season, specified by its
    start year and start month. The standard seasons are all three months long.
    The only start months accepted are 3, 6, 9, 12.

    The returned season specifier string is the month range, i.e.
        mYYYYMMYYYYMM

    """
    if startMonth not in [3, 6, 9, 12]:
        raise UnknownSeasonStartError("Season start month '%s' is not a standard season"%startMonth)

    endYear = startYear
    endMonth = startMonth + 2
    if endMonth > 12:
        endMonth = endMonth - 12
        endYear += 1

    season = "m%4.4d%2.2d%4.4d%2.2d" % (startYear, startMonth, endYear, endMonth)
    return season


def isSingleDate(filename):
    "Return True if 'when' field represents a single date"
    whenstr = when(filename)
    return (whenstr.isdigit() and len(whenstr) == 8)

def isNominalDate(filename):
    "Return True if 'when' field represents a nominal single date"
    whenstr = when(filename)
    return (whenstr.startswith('n') and len(whenstr) == 9 and whenstr[1:].isdigit())

def isYearRange(filename):
    "Return True if 'when' field represents a range of years"
    whenstr = when(filename)
    return (whenstr.startswith('y') and len(whenstr) == 9 and whenstr[1:].isdigit())

def isMonthRange(filename):
    "Return True if 'when' field is a range of months"
    whenstr = when(filename)
    return (whenstr.startswith('m') and len(whenstr) == 13 and whenstr[1:].isdigit())

def isDayRange(filename):
    "Return True if 'when' field is a range of individual days"
    whenstr = when(filename)
    return (whenstr.startswith('d') and len(whenstr) == 17 and whenstr[1:].isdigit())

def stage(filename):
    "Extract the processing stage of a QVF filename"
    return components(filename)[3][0:3]


def zonecode(filename):
    """
    Extract the UTM zone coding. Letter coding and single digit zone. If extended
    code is used, then the whole thing is returned.
    """
    return components(filename)[3][3:]


def suffix(fullpath):
    "Extract the filename suffix, i.e. the part after the first '.' in the filename"
    filename = os.path.basename(fullpath)
    dot1 = filename.find('.')
    return filename[dot1+1:]


def landsatnum(fullpath):
    "Extract the Landsat satellite number, or nothing for other satellites"
    sat = satellite(fullpath)
    if (sat >= "l0") and (sat <= "l9"):
        return sat[1:]


def utmzone(fullpath):
    """
    Extract the UTM zone number, as a two-digit string, e.g. "55"
    """
    code = zonecode(fullpath)
    zoneNumberStr = None
    if len(code) > 0 and code[0] == 'm':
        if len(code) == 2:
            if (code >= "m0") and (code <= "m9"):
                zoneNumberStr = "5"+code[1:]
        elif len(code) == 3:
            zoneNumberStr = code[1:]

    return zoneNumberStr

def optionfield(file, tag):
    "Returns value of option field beginning with tag"
    value = None
    for fld in components(file)[4:]:
        if fld[0] == tag:
            value = fld[1:]
    return value

def optionfielddict(file):
    "Returns a dictionary of tag-value pairs from the optionfields "
    dictn = {}
    for fld in components(file)[4:]:
        tag = fld[0]
        value = fld[1:]
        dictn[tag] = value
    return dictn

def assemble(components):
    "Reverse of components() function. Assembles a list of components with '_' between"
    return "_".join(components)


def changesuffix(fullpath, suffix):
    "Change the suffix of a QVF filename. Preserves other path components"
    (path, filename) = os.path.split(fullpath)
    base = filename.split('.')[0]
    newname = base+"."+suffix

    if path:
        return os.path.join(path, newname)
    else:
        return newname


def changebasename(fullpath, newbasename):
    "Change the basename part of a QVF filename. Probably only for internal use"
    (path, filename) = os.path.split(fullpath)
    dot1 = filename.find('.')
    if (dot1 != -1):
        suffix = filename[dot1:]
    else:
        suffix = ""
    return os.path.join(path, newbasename+suffix)


def changestage(fullpath, stage):
    "Change processing stage in a QVF filename. Preserves other path components"
    complist = components(fullpath)
    complist[3] = stage + complist[3][3:]
    newbase = assemble(complist)
    return changebasename(fullpath, newbase)



def changezonecode(fullpath, newzonecode):
    "Change zonecoding (2 characters) of a QVF filename. Preserves other path components"
    complist = components(fullpath)
    complist[3] = complist[3][0:3] + newzonecode
    newbase = assemble(complist)
    return changebasename(fullpath, newbase)


def createutmzonecode(zonenumber):
    """
    Create a zone code from a UTM zone number, with either 1 or 2 digits of zone number
    as required. Uses the 'm' coding.
    """
    zone = int(zonenumber)
    if 50 <= zone <= 59:
        codedZone = zone - 50
    else:
        codedZone = zone
    zoneStr = 'm%s'%codedZone
    return zoneStr


def changeutmzone(fullpath, zonenumber):
    """
    Change zone coding given a UTM zone number. Sets with 'm' zone code,
    with 1 or 2 digits as required
    """
    zoneStr = createutmzonecode(zonenumber)
    return changezonecode(fullpath, zoneStr)


def changeinstrument(fullpath, instrument):
    "Change the instrument field of a qvf filename. Preserves other path components"
    complist = components(fullpath)
    complist[0] = complist[0][0:2] + instrument + complist[0][4:]
    newbase = assemble(complist)
    return changebasename(fullpath, newbase)


def changeproduct(fullpath, product):
    "Change the product code of a qvf filename. Preserves other path components"
    complist = components(fullpath)
    complist[0] = complist[0][0:4] + product
    newbase = assemble(complist)
    return changebasename(fullpath, newbase)

def changesatellite(fullpath, satellite):
    "Change the satellite code of a qvf filename. Preserves other path components"
    complist = components(fullpath)
    complist[0] = satellite + complist[0][2:]
    newbase = assemble(complist)
    return changebasename(fullpath, newbase)


def changedate(fullpath, newdate):
    "Change the date part of a qvf filename. Preserves other path components"
    complist = components(fullpath)
    complist[2] = newdate
    newbase = assemble(complist)
    return changebasename(fullpath, newbase)

def changelocation(fullpath, newloc):
    "Change the location part of a qvf filename. Preserves other path components"
    complist = components(fullpath)
    complist[1] = newloc
    newbase = assemble(complist)
    return changebasename(fullpath, newbase)


def changeoptionfield(fullpath, tagChar, fieldVal, forcesorted=True):
    """
    Change or add a tagged option field. Pass None to delete.

    The forcesorted argument, if True, will force that all option
    fields in the filename are sorted in order of tag character. It
    can be set to False for historical compatability, as we did not always
    have it, but for any future uses it should always be left as True.

    """
    complist = components(fullpath)
    newoptions = []
    added = False
    for option in complist[4:]:
      if option[0] == tagChar:
        if fieldVal is not None:
          newoptions.append(tagChar + fieldVal)
          added = True
      else:
        newoptions.append(option)

    if not added and fieldVal is not None:
      # haven't seen this one - add it
      newoptions.append(tagChar + fieldVal)

    if forcesorted:
        newoptions = sorted(newoptions)

    complist = complist[0:4] + newoptions
    newbase = assemble(complist)
    return changebasename(fullpath, newbase)


ST_LANDSAT = "landsat"
ST_SPOT = "spot"
ST_AQUATERRA = "aquaterra"
ST_RAPIDEYE = "rapideye"
ST_ADS = "ads" # NSW
ST_ZY3 = 'zy3'
ST_SENTINEL2 = "sentinel2"
ST_MIXED = "mixed"

def genericSatellite(filename):
    """
    Returns a generic satellite group name for the given filename,
    e.g. landsat, spot, sentinel2.

    Possible names are listed as constants in this module, of the
    form ST_*

    Args:
        filename str(): QVF standard filename
    """
    sat = satellite(filename)
    if sat in ['l1', 'l2', 'l3', 'l4', 'l5', 'l7', 'l8', 'l9', 'lz']:
        sattype = ST_LANDSAT
    elif sat in ['s4', 's5', 's6', 's7']:
        sattype = ST_SPOT
    elif sat in ['a1', 't1', 'ta']:
        sattype = ST_AQUATERRA
    elif sat in ['rp']:
        sattype = ST_RAPIDEYE
    elif sat in ['ad']:
        sattype = ST_ADS
    elif sat in ['z3']:
        sattype = ST_ZY3
    elif sat in ['ce', 'cf', 'cg', 'ch', 'cv']:
        sattype = ST_SENTINEL2
    elif sat == 'mx':
        sattype = ST_MIXED
    else:
        sattype = None
    return sattype


def isLandsat(filename):
    "Return True if filename is for Landsat"
    return (genericSatellite(filename) == ST_LANDSAT)

def isLandsatOLI(filename):
    "Return True if filename is for Landsat OLI"
    return (isLandsat(filename) and (instrument(filename) == "ol"))

def isLandsatTM(filename):
    "Return True if filename is for Landsat TM"
    return (isLandsat(filename) and (instrument(filename) == "tm"))

def isLandsatSLCoff(filename):
    "Return True if filename is for a Landsat-7 SLC-off image"
    sat = satellite(filename)
    date = when(filename)
    landsat7_SLCfailDate = "20030531"
    return (sat == "l7" and date >= landsat7_SLCfailDate)

def isLandsatMSS(filename):
    "Return True if filename is for Landsat MSS"
    return (isLandsat(filename) and instrument(filename) == "ms")

def isSpot(filename):
    "Return True if filename is for SPOT"
    return (genericSatellite(filename) == ST_SPOT)

def isSpotHG(filename):
    "Return True if filename is for SPOT HG instrument"
    return (isSpot(filename) and (instrument(filename) == "hg"))

def isSpotNI(filename):
    "Return True if filename is for SPOT NI (NAOMI) instrument"
    return (isSpot(filename) and (instrument(filename) == "ni"))

def isRapidEye(filename):
    "Return True if the filename is for Rapid Eye"
    return (genericSatellite(filename) == ST_RAPIDEYE)

def isAquaTerra(filename):
    "Return True if filename is for aqua or terra)"
    return (genericSatellite(filename) == ST_AQUATERRA)

def isMODIS(filename):
    "Return True if filename is for the MODIS instrument (aqua or terra)"
    return (isAquaTerra(filename) and (instrument(filename) == "mo"))

def isMISR(filename):
    "Return True if filename is for MISR instrument"
    return ((satellite(filename) == "t1") and (instrument(filename) == "mi"))

def isALOS(filename):
    "Return True if filename is for a PALSAR instrument"
    return ((satellite(filename) == "t1") and (instrument(filename) == "mi"))

def isPALSAR(filename):
    "Return True if filename is for a PALSAR instrument"
    return (isALOS(filename) and (instrument(filename) == "ps"))

def isADS(filename):
    "Return True if filename is for the ADS airborne platform"
    return (genericSatellite(filename) == ST_ADS)

def isZY3(filename):
    "Return True if filename is for the ZY3 satellite"
    return (genericSatellite(filename) == ST_ZY3)

def isSentinel2(filename):
    "Return True if filename is for the Copernicus Sentinel-2 satellite"
    return (genericSatellite(filename) == ST_SENTINEL2)

def isQldSentinel2(filename):
    """
    Return True if the tile is one of the tiles that lie partially in Qld

    Will return False if not sentinel, even if it lies in Queensland.

    """
    return (locationid(filename) in QLD_SENTINEL_TILES)



def isMixedSat(filename):
    """
    Return True if filename is derived from a mix of different
    satellite data.
    """
    return (genericSatellite(filename) == ST_MIXED)


def isCRFfilename(filename):
    """
    Return True if filename has a Conditional Random  Fields product filename
    """
    return (
        fnmatch(locationid(filename), "h[0-9][0-9]v[0-9][0-9]") and
        (
            isMixedSat(filename) or
            isLandsat(filename) or
            isSentinel2(filename)
        )
    )


def parfilename(fullpath):
    """
    Change several components of the filename, to make a parfile name. Uses other
    QVF commands to change stage, zonecode and suffix to aa0f0.par
    """
    newfile = changestage(fullpath, "aa0")
    newfile = changezonecode(newfile, "f0")
    newfile = changesuffix(newfile, "par")
    return newfile


def scenegroup(file):
    """
    Return the scenegroup in which the scene should be stored. Initially implemented
    for Landsat, but will apply to other things.
    """
    satid = satellite(file)
    instr = instrument(file)
    prod = product(file)
    sceneGroup = None
    if satid in ['l1', 'l2', 'l3']:
        sceneGroup = "landsat123"
    elif satid in ['l4', 'l5', 'l7', 'lz', 'l8', 'l9']:
        if instr == "ms":
            sceneGroup = "landsat45mss"
        else:
            sceneGroup = "landsat57tm"
    elif satid in ['s5', 's4']:
        if prod == "sm":
            sceneGroup = "spotmaps"
    elif satid == "t1":
        latCodes = ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L")
        if instr == "as":
            locid = locationid(file)
            lon = 100.0 + float(locid[0:3])/10
            lat = float(locid[3:6])/10
            latNdx = int(lat/4)
            zone = 31 + int(lon/6)
            mapSheet = "S%s%d" % (latCodes[latNdx], zone)
            subSheet = "%d" % (int((lat%4)/2)*2 + int(lon%6/3) + 1)
            sceneGroup = os.path.join(mapSheet, subSheet)
    elif satid == 'z3':
        sceneGroup = 'zy3'
    return sceneGroup

dbCon = None
dbCursor = None
def scenestate(scene):
    "Returns the state that the scene is stored under"
    global dbCursor, dbCon
    if dbCursor is None:
        from rsc.utils import metadb
        dbCon = metadb.connect()
        dbCursor = dbCon.cursor()
    dbCursor.execute("SELECT state FROM landsat_state WHERE scene = '%s'" % scene[0:4])
    resultList = dbCursor.fetchall()
    if len(resultList) > 0:
        result = resultList[0]
    else:
        raise UnknownAustStateError("Scene %s is not in any Australian state" % scene)
    state = result[0]
    return state


# A set of symbols for the top-level directories for different
# satellite/sensor groups. All symbols begin with TOPDIR_ (so they will group together
# in the help), and are relative to the collection.
TOPDIR_LANDSAT_TM_WRS2 = "landsat/landsat57tm/wrs2"
# For old 4-character scene names. I wish this were less general.
TOPDIR_LANDSAT_TM_4CHAR = "landsat/landsat57tm"
TOPDIR_LANDSAT_CRFGRID = "landsat/landsat57tm"
# This is now obsolete
TOPDIR_LANDSAT_TM_BYLATLONG = "landsat/landsat57tm"
TOPDIR_SPOT = "spot"
TOPDIR_SPOTMAPS = "spot/spotmaps"
TOPDIR_PALSAR = "alos/palsar"
TOPDIR_MODIS = "modis"
TOPDIR_ZY3 = "zy3"
TOPDIR_SENTINEL2 = "sentinel2"
TOPDIR_EARTHI = "earthi"
TOPDIR_AIRBORNELIDAR = "airbornelidar"
TOPDIR_MULTISAT = "multisat"
TOPDIR_AIRBORNEMULTI = "airbornemulti"

MOSAICNAMES_STATES = set(['qld', 'vic', 'nsw', 'nt', 'wa', 'sa', 'tas', 'aus'])

def isMosaic(locId):
    """
    Return True if the given location id field appears to be for a well-known
    mosaic.

    Currently only knows about the state names.

    NSW have overridden this in qvf_site_NSW.py as they have
    additional and specific cases.

    """
    status = False
    if locId in MOSAICNAMES_STATES:
        status = True
    elif locId.startswith('r') and len(locId) > 4:
        status = True

    return status



def decomposeEraName(era):
    """
    Decompose a SLATS-style era name into a pair of years.
    Returns a tuple of integer values
        (startYear, endYear)

    """
    if not era.startswith('e'):
        raise EraNameError("Era name '%s' should start with 'e'" % era)

    startYear = year2dTo4d(era[1:3])
    endYear = year2dTo4d(era[3:5])

    return (startYear, endYear)


def year2dTo4d(year2d):
    """
    Return a 4-digit year, given the 2-digit year. Assumes
    that any 2-digit year < 50 is from 2000+, and anything >= 50
    is from 1900+.

    Returns the value as an integer, regardless of the type of
    the 2-digit year given.

    """
    year2d = int(year2d)
    if year2d < 50:
        year4d = 2000 + year2d
    else:
        year4d = 1900 + year2d
    return year4d


class Season(object):
    """
    Represent a qvf-style season, with methods for stepping forward and backwards.

    Can be either a month range (starting with 'm') or a single month (yyyymm),
    or a single year (yyyy).
    The single-month and single year variants are required because the seasonal
    composite processing system is being adapted to manage composites for
    single months and whole calendar years, and we want all this to look
    seamless, while still being consistent with previous conventions for
    QVF naming.
    """
    def __init__(self, seasonStr=None, startYear=None, startMonth=None,
            endYear=None, endMonth=None):
        """
        Give either a season string (either month range or single month)
        or the individual start/end year/months
        """
        if seasonStr is not None:
            seasonStr = str(seasonStr)      # In case we have been given a Season object
            self.seasonStr = seasonStr
            if not seasonStr.startswith('m'):
                if len(seasonStr) == 6:
                    self.startYear = int(seasonStr[:4])
                    self.startMonth = int(seasonStr[4:6])
                    self.endYear = self.startYear
                    self.endMonth = self.startMonth
                elif len(seasonStr) == 4:
                    self.startYear = int(seasonStr)
                    self.endYear = self.startYear
                    self.startMonth = 1
                    self.endMonth = 12
            else:
                self.startYear = int(seasonStr[1:5])
                self.startMonth = int(seasonStr[5:7])
                self.endYear = int(seasonStr[7:11])
                self.endMonth = int(seasonStr[11:13])
        else:
            self.startYear = int(startYear)
            self.startMonth = int(startMonth)
            self.endYear = int(endYear)
            self.endMonth = int(endMonth)
            if self.startYear == self.endYear and self.startMonth == self.endMonth:
                self.seasonStr = '{:04}{:02}'.format(self.startYear, self.startMonth)
            elif self.startYear == self.endYear and self.startMonth == 1 and self.endMonth == 12:
                self.seasonStr = '{:04}'.format(self.startYear)
            else:
                self.seasonStr = 'm{:04}{:02}{:04}{:02}'.format(self.startYear, self.startMonth,
                        self.endYear, self.endMonth)
        self.nMonths = ((12 - self.startMonth + 1) +
                        (self.endYear - self.startYear - 1) * 12 +
                        self.endMonth)

    def next(self, step=1, yearstep=None):
        """
        Return a new Season object for the season following this one
        by <step> seasons. Default step is 1, so this gives the immediately
        following season.

        If step is negative, will step backwards by this many seasons.

        If yearstep is not None, the step will be this many years, rather
        than a number of seasons, allowing stepping to the same season
        in a different year. This can also be negative for stepping backwards.

        """
        if yearstep is not None:
            monthIncr = yearstep * 12
        else:
            monthIncr = step * self.nMonths
        (startYear, startMonth) = self.monthIncr(self.startYear, self.startMonth,
            monthIncr)
        (endYear, endMonth) = self.monthIncr(startYear, startMonth, self.nMonths-1)
        newSeas = Season(startYear=startYear, startMonth=startMonth,
            endYear=endYear, endMonth=endMonth)
        return newSeas

    @staticmethod
    def monthIncr(year, month, incr):
        """
        Increment the given month by <incr> months. Return (year, month).
        Accepts negative increments for decrement
        """
        monthPlus = month + incr
        deltaYear = (monthPlus - 1) // 12
        newYear = year + deltaYear
        newMonth = (monthPlus - 1) % 12 + 1
        return (newYear, newMonth)

    def isEquivalent(self, other):
        """
        Return True if the other season is equivalent to the current season,
        in the sense of being the same season for a different year. Checks
        length and start month.
        """
        isEquiv = ((self.nMonths == other.nMonths) and
            (self.startMonth == other.startMonth))
        return isEquiv

    def genericSeasonStr(self):
        """
        Return a string indicating the generic season (independent of the year)
        """
        s = None
        if self.nMonths == 1:
            s = self.seasonStr[4:6]
        elif self.nMonths == 3:
            s = self.seasonStr[5:7] + self.seasonStr[11:13]
        return s

    def __str__(self):
        return self.seasonStr

    def __repr__(self):
        return "qvf.Season('{}')".format(self.seasonStr)

    # Allow ordering comparisons. Includes comparisons with a string season name.
    # Note that comparisons of incomparable seasons will behave weirdly,
    # should trap these, but there are many cases to check.
    def __gt__(self, other):
        return str(self) > str(other)

    def __lt__(self, other):
        return str(self) < str(other)

    def __ge__(self, other):
        return str(self) >= str(other)

    def __le__(self, other):
        return str(self) <= str(other)

    def __eq__(self, other):
        return str(self) == str(other)

    def __ne__(self, other):
        return str(self) != str(other)

    def __hash__(self):
        return hash(str(self))

# Exception classes
class QvfError(Exception):
    "Base class for qvf exceptions"
    pass
class UnknownAustStateError(QvfError):
    "A scene name does not correspond to a state (i.e. qld, nsw, etc.)"
    pass
class MissingComponentError(QvfError):
    "Filename has less than 4 components (i.e. the bits separated by '_')"
    pass
class NoQvladmSubdirError(QvfError):
    "Could not work out which subdir in which store the given file"
    pass
class InvalidWhenTypeError(QvfError): pass
class UnknownSeasonStartError(QvfError): pass
class EraNameError(QvfError): pass

