import platform

import fractionalcover3 as fcv3
from prefect import flow, task, get_run_logger
import os

import numpy
from time import sleep

@task
def take_up_ram(gigabytes):
    logger = get_run_logger()
    n = gigabytes * 1024
    result = [numpy.random.bytes(1024*1024) for x in range(n)]
    logger.info(f"""

    RAM use simulate
    ----------------
    simulating {gigabytes} gig

    """
    )


@task
def get_version():
    logger = get_run_logger()
    logger.info(f"""


        Fractional cover 3 version
        --------------------------
        {fcv3.__version__}
        """)

    return 0


@task
def get_host_info():
    logger = get_run_logger()
    logger.info(
        f"""


    Host Information
    ----------------
    machine: {platform.machine()}
    node: {platform.node()}
    platform: {platform.platform()}
    workingdir: {os.getcwd()}
    """
    )

    return 0

@task(name="some env vars")
def get_env_vars():
    logger = get_run_logger()
    logger.info(
        f"""
        ENVIRONMENT
        -----------
        http_proxy={os.getenv('http_proxy')}
        https_proxy={os.getenv('https_proxy')}
        no_proxy={os.getenv('no_proxy')}
        """
    )
    return 0




@flow(name='Example Flow', log_prints=True)
def env_info():
    """
    Example flow
    """
    get_host_info()
    get_version()
    get_env_vars()
    take_up_ram(3)
    # make it run for 10mins
    sleep(5*60)
    return 0


if __name__ == '__main__':
    # create your first deployment
    env_info.serve(name='ASDI Test flow')
