import rsc.qvf as qvf
from prefect import flow
from prefect.deployments import run_deployment
from utils import yearmon_to_daterange

@flow(name='qld season run')
def qld_tile_flow(startmonth, endmonth):
    startdate, _ = yearmon_to_daterange(startmonth)
    _, enddate = yearmon_to_daterange(endmonth)
    for tile in qvf.QLD_SENTINEL_TILES:
        myparams = {
            'startmonth': startmonth,
            'endmonth': endmonth,
            'tile': tile,
        }
        run_deployment('season run/seasonrun', parameters=myparams, timeout=0)

@flow(name='sub qld season run', description="shorter run for testing")
def qld_sub_tile_flow(startmonth, endmonth):
    startdate, _ = yearmon_to_daterange(startmonth)
    _, enddate = yearmon_to_daterange(endmonth)
    for tile in qvf.QLD_SENTINEL_TILES[0:100]:
        myparams = {
            'startmonth': startmonth,
            'endmonth': endmonth,
            'tile': tile,
        }
        run_deployment('season run/seasonrun', parameters=myparams, timeout=0)
