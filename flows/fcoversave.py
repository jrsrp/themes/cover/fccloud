#!/bin/env python
"""
save the three input files to the correct destination
according to our filenaming scheme.

Add an entry in the stac catalog
"""

import json
import tempfile
from datetime import timedelta

import boto3
import pystac
import requests
from auscophub import sen2meta
from prefect import get_run_logger, task
from pystac_client import Client
from rasterio.io import MemoryFile
from rio_cogeo.cogeo import cog_translate
from rio_cogeo.profiles import cog_profiles
from shapely.geometry import Polygon
import qvf_common as qvf

STAGE_CODE = "aj8"
STAC_SERVER= "http://espb4stac.dnr.qld.gov.au/api/v1/pgstac/"

def get_metadata(href):
    """
    Get the angle info from an xml file from a url.
    """
    with tempfile.NamedTemporaryFile(mode="w+b", delete=True) as tmpfh:
        r = requests.get(href, allow_redirects=True)
        tmpfh.write(r.content)
        tmpfh.flush()
        info = sen2meta.Sen2TileMeta(filename=tmpfh.name)
    return info


@task()
def delete_item(stacid, username, password):
    """
    Delete the item from our catalogue if it exists

    To separate the ids from official ones and our
    test ones, our local stacid has '_asdi' appended.
    """
    catalog = Client.open(STAC_SERVER)
    search = catalog.search(max_items=10, ids=stacid, method="GET")
    items = search.get_all_items()
    logger = get_run_logger()
    if len(items) == 0:
        logger.info(f"could not find {stacid} in catalogue")
        logger.info("nothing to delete")
        return 0
    item = items[0]
    href = item.self_href
    session = requests.Session()
    session.auth = (username, password)
    session.headers.update(
        {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    )
    response = session.delete(href)
    if response.ok:
        logger.info(f"item deleted: {response.json()}")
    else:
        logger.error(f"response returned {response.status_code}:{response.reason}")
        response.raise_for_status()

def stac_uri(satid, fname):
    """
    Given a satid and a qvf style name,
    construct the uri
    """
    sequence = satid.split("_")[3]
    if int(sequence) > 0:
        fname = qvf.changeoptionfield(fname, "i", sequence)
    # our path is
    # sentinel2/55/JKM/2022/01/S2B_56JMT_20200424_1_L2A/
    zone = qvf.locationid(fname)[1:3]
    tile = qvf.locationid(fname)[3:6]
    year = qvf.when(fname)[0:4]
    month = qvf.when(fname)[4:6]

    uripath = f"sentinel2/{zone}/{tile}/{year}/{year}{month}/{satid}/{fname}"
    return str(uripath)


@task(name="check whether satid is there if present")
def get_local_id(satid, catalogue):
    """
    Just check to see if it is already a record on our stac
    before processing. Will return the stac id if it exists
    """
    logger = get_run_logger()
    catalog = Client.open(catalogue)
    logger.info(f"opening STAC {catalog.title}")
    search = catalog.search(collections=["sentinel-2-l2a"], ids=[satid])
    items = search.get_all_items()
    if len(items) == 0:
        logger.error(f"ERROR: didn't find any records matching {satid}")
    selected_item = items[0]
    geom = selected_item.geometry
    centroid = Polygon(geom['coordinates'][0]).centroid
    logger.info(f"retrieved item for satid {satid}")
    # now search ours for a similar one
    catalog = Client.open(STAC_SERVER)
    logger.info(f"Opening local catalog {catalog.title}")
    localsearch = catalog.search(
        max_items=2000,
        collections=["sentinel2_sdt_asdi"],
        intersects=centroid,
        datetime=[
            selected_item.datetime - timedelta(days=1),
            selected_item.datetime + timedelta(days=1),
        ]
    )
    items = localsearch.get_all_items()
    logger.info(f"found {len(items)} possiblle matches")
    for item in items:
        if item.properties["productIdentifier"] == satid:
            logger.info(f"found {item.id}")
            return item.id
    return None


@task(name="save_fcover")
def save_fcover(
    satid, bare, pv, npv, catalogue, bucket, aws_access_key_id, aws_secret_access_key
):
    """
    Save bare, pv, npv images to S3, with metadataa derived
    from the xml corresponding to the satid identifier.
    satid looks like 'S2A_55HBB_20210530_0_L2A'

    Will use either profile name, or aws keys, or default
    """
    # setup s3 access
    logger = get_run_logger()
    logger.info("accessing AWS")
    session = boto3.Session(
        aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key
    )
    s3 = session.client("s3")

    catalog = Client.open(catalogue)
    logger.info(f"opening STAC {catalog.title}")
    search = catalog.search(collections=["sentinel-2-l2a"], ids=[satid])
    items = search.get_all_items()
    selected_item = items[0]
    logger.info(f"retrieved item for satid {satid}")
    xmlref = selected_item.assets["granule_metadata"].href
    info = get_metadata(xmlref)

    baseQVFname = info.makeQVFname()
    baseQVFimgName = qvf.changesuffix(baseQVFname, "tif")
    #  for now lets use aj8
    baseQVFimgName = qvf.changestage(baseQVFimgName, STAGE_CODE)
    # we use options to set bare, pv, npv

    # and destination path
    # if we set the FILESTORE_PATH
    # to s3://esp-rss-imagerepo/
    dst_profile = cog_profiles.get("deflate")
    for infile, band in zip((bare, pv, npv), ("bare", "pv", "npv")):
        # we use 'b' for band and
        tifname = qvf.changeoptionfield(baseQVFimgName, "b", band)
        #  and 'i' for id
        full_key = stac_uri(satid, tifname)
        # cogify before upload
        with MemoryFile() as mem_dst:
            # Important, we pass `mem_dst.name` as output dataset path
            cog_translate(infile, mem_dst.name, dst_profile, in_memory=True)
            # You can then use the memoryfile to do something else like
            # upload to AWS S3
            # everything I read says this will overwrite if it has
            # the samem key
            s3.upload_fileobj(mem_dst, bucket, str(full_key))
            logger.info(f"uploaded {full_key} to {bucket}")
            # add the record to the STAC

    # also create a 3band thumbnail


@task(name="update STAC")
def update_stac(element84satid, localsatid, username, password):
    """
    Update the stac catalog
    pgstac allows us to use the original satid as the id
    """
    logger = get_run_logger()
    awscatalog = Client.open("https://earth-search.aws.element84.com/v1/")
    awssearch = awscatalog.search(collections=["sentinel-2-l2a"], ids=[element84satid])
    items = awssearch.get_all_items()
    selected_item = items[0]
    logger.debug(f"found item for satid {element84satid}")
    newitem = pystac.Item(
        id=localsatid,
        geometry=selected_item.geometry,
        bbox=selected_item.bbox,
        datetime=selected_item.datetime,
        properties=selected_item.properties,
    )
    xmlref = selected_item.assets["granule_metadata"].href
    info = get_metadata(xmlref)
    baseQVFname = info.makeQVFname()
    baseQVFimgName = qvf.changesuffix(baseQVFname, "tif")
    #  for now lets use aj8
    baseQVFimgName = qvf.changestage(baseQVFimgName, STAGE_CODE)

    ## add the assets
    for band in ("bare", "pv", "npv"):
        tifname = qvf.changeoptionfield(baseQVFimgName, "b", band)
        full_key = "s3://esp-rss-imagerepo/" + stac_uri(element84satid, tifname)
        #logger.info(f"adding asset {band} with key {full_key} to item")
        newitem.add_asset(
            key=band, asset=pystac.Asset(href=full_key, media_type=pystac.MediaType.COG)
        )
    # now open our catalogue
    catalog = Client.open(STAC_SERVER)
    logger.info(f"Opening local catalog {catalog.title}")

    s2coll = catalog.get_collection("sentinel2_sdt_asdi")
    href = f"{s2coll.self_href}/items/"
    newitem.set_self_href(href)

    # now update
    json_object = json.dumps(newitem.to_dict(), indent=4)

    href = f"{s2coll.self_href}/items/"
    session = requests.Session()
    session.auth = (username, password)
    session.headers.update(
        {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    )

    response = session.post(href, data=json_object)
    if response.ok:
        logger.info(f"item added: {response.json()}")
    else:
        logger.error(f"response returned {response.status_code}:{response.reason}")
        response.raise_for_status()

