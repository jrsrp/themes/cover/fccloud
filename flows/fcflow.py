from prefect import flow, get_run_logger, task
from prefect.blocks.system import Secret
from fcover import compute_fcover
from fcoversave import save_fcover, delete_item, update_stac
import tempfile
from pathlib import Path
import os

aws_secret = Secret.load("imgreposecret")
aws_key = Secret.load("imgrepokey")

stac_user = Secret.load("stac-user")
stac_password = Secret.load("stac-password")

@task(name="fcv3")
def make_fcover(satid):
    """wrapper around fc_s2l2a:compute_fcover"""
    # we return sati and names of the temp files
    # so we can use this information to save things

    logger = get_run_logger()
    tmpdir = tempfile.mkdtemp(dir=".")
    bare_fname = Path(tmpdir) / "bare.tif"
    pv_fname = Path(tmpdir) / "pv.tif"
    npv_fname = Path(tmpdir) / "npv.tif"
    logger.info(f"{bare_fname=}")
    # you could put here your ceph stuff
    retcode = compute_fcover(satid, str(bare_fname), str(pv_fname), str(npv_fname),
         "https://earth-search.aws.element84.com/v1/")
    logger.info(f"compute_fcover finished with {retcode=}")
    return (satid, str(bare_fname), str(pv_fname), str(npv_fname))


@flow(
    name="fcv3_single_date",
    description="""A flow which creates the single date fractional cover version
3 image, copies it to the S3 bucket, and updates the local stac catalog""",
    retries=0,
    retry_delay_seconds=300,
    timeout_seconds=3600
)
def fc_flow(satids):
    """run multiple instances of single date flows

    Args:
        satids (str): the satids you want to process
    """
    # try this concurrently
    logger = get_run_logger()
    # now make the fcover, and return the names
    cover_tasks = []
    for satid in satids:
        cover_tasks.append(make_fcover.submit(satid))
    # assuming they all worked, save each one
    for ctask in cover_tasks:
        (satid, bare_fn, pv_fn, npv_fn) = ctask.result()
        save_fcover.submit(
            satid,
            bare_fn,
            pv_fn,
            npv_fn,
            "https://earth-search.aws.element84.com/v1/",
            "esp-rss-imagerepo",
            aws_key.get(),
            aws_secret.get(),
        )
    # and update all the records
    for satid in satids:
        noproxy = os.getenv("no_proxy")
        logger.info(f"no proxy setting is {noproxy}")
        # delete if it exists
        logger.info(f"updating stac for item {satid}")
        # local id
        localsatid = f"{satid}_asdi"
        # check no-proxy

        delete_item(localsatid, stac_user.get(), stac_password.get())
        update_stac(satid, localsatid, stac_user.get(), stac_password.get())

if __name__ == '__main__':
    # create your first deployment
    fc_flow.serve(name='ASDI Test fcflow')