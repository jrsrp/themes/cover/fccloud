"""
An FCv3 Median flow
"""
import json
from datetime import datetime
from pathlib import Path

import boto3
import numpy as np
import pyproj
import pystac
import rasterio as rio
import requests
import rsc.qvf as qvf
from botocore.exceptions import ClientError
from prefect import flow, get_run_logger, task
from prefect.blocks.system import Secret
from prefect_shell import shell_run_command
from pyproj import CRS, Proj
from pystac.extensions.projection import ProjectionExtension
from pystac_client import Client
from rasterio.io import MemoryFile


from rio_cogeo.cogeo import cog_translate
from rio_cogeo.profiles import cog_profiles
from shapely.geometry import Polygon, box
from shapely.geometry import mapping as geojson_mapping
from shapely.geometry import shape
from shapely.ops import transform
from prefect.server.schemas.states import StateType

from qvmed import create_median_image, partition, snap_value
from fcoversave import delete_item
from utils import get_geom_epsg, yearmon_to_daterange, get_items
from prefect import variables

NONCURRENTMEDIAN = int(variables.get('nconcurrentmedian'))

STAC_SERVER= "http://espb4stac.dnr.qld.gov.au/api/v1/pgstac/"
aws_secret = Secret.load("imgreposecret")
aws_key = Secret.load("imgrepokey")
rss_bucket = "esp-rss-imagerepo"

stac_user = Secret.load("stac-user")
stac_password = Secret.load("stac-password")


def number_running(tasklist):
    """
    simple check for the number of
    running tasks in the list.
    """
    nrunning = 0
    for taskjob in tasklist:
        if taskjob.get_state().type == StateType.RUNNING:
            nrunning += 1
    return nrunning





@task(name="update STAC for qvmedian")
def update_stac_median(bare, username, password):
    """
    Update the stac catalog for a median image
    this is a 3 monthly product
    """
    logger = get_run_logger()
    # get the geometry from the actual image, safer than
    # from database
    # should use the stacutils approach

    with rio.open(bare) as dataset:
        cog_bbox = list(dataset.bounds)
        cog_transform = list(dataset.transform)
        cog_shape = [dataset.height, dataset.width]

        transformer = Proj.from_crs(dataset.crs, CRS.from_epsg(4326), always_xy=True)
        bbox = list(
            transformer.transform_bounds(
                dataset.bounds.left,
                dataset.bounds.bottom,
                dataset.bounds.right,
                dataset.bounds.top,
            )
        )
        geometry = geojson_mapping(box(*bbox, ccw=True))

    # Create item

    start_datetime = datetime.strptime(f"{qvf.when(bare)[1:7]}01", "%Y%m%d")
    end_datetime = datetime.strptime(f"{qvf.when(bare)[7:13]}01", "%Y%m%d")

    DATA_EPSG = int(dataset.crs["init"].lstrip("epsg:"))
    DATA_CRS_WKT = dataset.crs.wkt

    properties = {
        "description": "Monthly fractional cover v3",
        "created": f"{datetime.today()}",
        "proj:epsg": DATA_EPSG,
        "mgrs:utm_zone": int(qvf.locationid(bare)[1:3]),
        "mgrs:latitude_band": qvf.locationid(bare)[3].upper(),
        "mgrs:grid_square": qvf.locationid(bare)[4:6].upper(),
        "grid:code": f"'MGRS-{qvf.locationid(bare)[1:].upper()}",
    }

    majortile = qvf.locationid(bare)[0:4]
    fulltile = qvf.locationid(bare)
    season = qvf.when(bare)
    uripath = f"sentinel2/{majortile}/{fulltile}/{season}/"

    # should be no duplicate ids, so we can use
    # a combination of tile and season
    item = pystac.Item(
        id=f"{fulltile}_{season}_asdi",
        geometry=geometry,
        bbox=bbox,
        datetime=start_datetime,
        properties=properties,
        stac_extensions=[],
    )

    item.common_metadata.start_datetime = start_datetime
    item.common_metadata.end_datetime = end_datetime

    for bandname in ("bare", "pv", "npv"):
        fname = bare.replace(".vrt", ".tif")  # in case you gave it a vrt
        fname = fname.replace("bbare", f"b{bandname}")
        full_key = "s3://esp-rss-imagerepo/" + uripath + fname

        cog_asset = pystac.Asset(
            href=full_key,
            media_type=pystac.MediaType.COG,
            roles=[
                "data",
                "labels",
                "labels-raster",
            ],
            title=f"monthly fractional cover median image for band {bandname}",
        )
        item.add_asset(bandname, cog_asset)

    # extensions
    item_projection = ProjectionExtension.ext(item, add_if_missing=True)

    item_projection.epsg = DATA_EPSG
    item_projection.wkt2 = DATA_CRS_WKT
    item_projection.bbox = cog_bbox
    item_projection.transform = cog_transform
    item_projection.shape = cog_shape

    catalog = Client.open(STAC_SERVER)
    logger.info(f"Opening local catalog {catalog.title}")

    s2coll = catalog.get_collection("sentinel2_sct_asdi")
    # check this, does it need the id or not
    href = f"{s2coll.self_href}/items/"
    item.set_self_href(href)

    # now update
    json_object = json.dumps(item.to_dict(), indent=4)
    session = requests.Session()
    session.auth = (username, password)
    session.headers.update(
        {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    )
    response = session.post(href, data=json_object)
    if response.ok:
        logger.info(f"item added: {response.json()}")
    else:
        logger.error(f"response returned {response.status_code}:{response.reason}")
        response.raise_for_status()


@task(name="save_median_fcover", retries=0)
def save_fcover(vrtname, bucket, aws_access_key_id, aws_secret_access_key):
    """
    convert vrt to cog and upload
    """
    # setup s3 access
    logger = get_run_logger()
    logger.info("accessing AWS")
    session = boto3.Session(
        aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key
    )
    s3 = session.client("s3")
    # recreate the path on filestore

    majortile = qvf.locationid(vrtname)[0:4]
    fulltile = qvf.locationid(vrtname)
    season = qvf.when(vrtname)
    uripath = f"sentinel2/{majortile}/{fulltile}/{season}/"
    logger.info(f"path  for new image is {uripath}")
    # and destination path
    # if we set the FILESTORE_PATH
    # to s3://esp-rss-imagerepo/
    dst_profile = cog_profiles.get("deflate")
    destfile = Path(vrtname).name.replace(".vrt", ".tif")
    #full_key = "s3://esp-rss-imagerepo/" + uripath + destfile
    full_key = uripath + destfile
    logger.info(f"full key on aws is {full_key}")
    # cogify before upload
    # I think I have to do this diffeerently,
    # its not a good use of ram
    with rio.Env(GDAL_NUM_THREADS=2, GDAL_CACHEMAX=100):
        with MemoryFile() as mem_dst:
            # Important, we pass `mem_dst.name` as output dataset path
            cog_translate(vrtname, mem_dst.name, dst_profile, in_memory=False)
            # You can then use the memoryfile to do something else like
            # upload to AWS S3
            try:
                response = s3.upload_fileobj(mem_dst, bucket, str(full_key))
            except ClientError as e:
                logger.error(e)
                raise e
            logger.info(f"uploaded {full_key} to {bucket}")
            # add the record to the STAC


def grid_bounds(geom, delta, extra=10):
    """
    Split a polygon up, evenly, but last one might be a bit bigger
    """
    minx, miny, maxx, maxy = geom.bounds
    minx = snap_value(minx) - extra
    maxx = snap_value(maxx) + extra
    miny = snap_value(miny) - extra
    maxy = snap_value(maxy) + extra

    nx = int((maxx - minx) / delta) + 1
    ny = int((maxy - miny) / delta) + 1
    gx, gy = minx + np.arange(nx) * delta, miny + np.arange(ny) * delta
    gx[-1] = maxx
    gy[-1] = maxy
    grid = []
    for i in range(len(gx) - 1):
        for j in range(len(gy) - 1):
            poly_ij = Polygon(
                [
                    [gx[i], gy[j]],
                    [gx[i], gy[j + 1]],
                    [gx[i + 1], gy[j + 1]],
                    [gx[i + 1], gy[j]],
                ]
            )
            grid.append(poly_ij)
    return grid


@flow(name="do median", retries=2, timeout_seconds=2700)
def create_fcv3_median(tile, startmonth, endmonth):
    """Generate the median fractional cover image for he tile

    Args:
        tile (string): the tile name (eg t55jdn). You need this to build the name
        startdate (string): the start date, like '20230101'
        enddate (string): the end date, like '20230131'
    """
    # first we do the median
    logger = get_run_logger()
    logger.info("running median")
    geom, epsgcode = get_geom_epsg(tile)

    startdate, _ = yearmon_to_daterange(startmonth)
    _, enddate = yearmon_to_daterange(endmonth)
    zonecode = tile[2] # t56jdn zone code is 6
    whenstr = f"m{startmonth}{endmonth}"

    # we'll create a filename
    zonecode = tile[2]
    barename = f"cfmsre_{tile}_{whenstr}_aj9m{zonecode}_bbare"
    pvname = f"cfmsre_{tile}_{whenstr}_aj9m{zonecode}_bpv"
    npvname = f"cfmsre_{tile}_{whenstr}_aj9m{zonecode}_bnpv"

    # we are going to split the geom up into pieces
    # first we split the geom up into chunks
    # first get it into destination coordinates
    poly4326 = shape(json.loads(geom))
    dst_crs = CRS.from_epsg(epsgcode)
    src_crs = CRS.from_epsg(4326)

    project = pyproj.Transformer.from_crs(src_crs, dst_crs, always_xy=True).transform
    backproject = pyproj.Transformer.from_crs(
        dst_crs, src_crs, always_xy=True
    ).transform
    poly = transform(project, poly4326)

    grid = partition(poly, 10000)

    # now iterate over each polygon
    # we are createing a number of images, so we
    # keep track of these. Also need to
    # wait until all the tasks finish, so
    # we keep trackof those too

    # we'll prepare lists so we can use prefec to batch
    # to map

    # we need a list for each
    #subpoly, epsgcode, items, baresubname, pvsubname, npvsubname
    # subploy = grid
    # epsgcode is fixed
    # items is a list of lists
    # then filenames

    epsgcodes = []
    all_items = []
    baresubnames = []
    pvsubnames = []
    npvsubnames = []

    for counter, subpoly in enumerate(grid):
        # get the poly back in the 4326
        sub4326 = transform(backproject, subpoly)
        items = get_items(tile, sub4326, startdate, enddate)

        logger.info(f"there are {len(items)} in subset {counter} of {len(grid)}")
        # get the name
        baresubname = f"{barename}_{counter:03}.tif"
        pvsubname = f"{pvname}_{counter:03}.tif"
        npvsubname = f"{npvname}_{counter:03}.tif"
        epsgcodes.append(epsgcode)
        all_items.append(items)
        baresubnames.append(baresubname)
        pvsubnames.append(pvsubname)
        npvsubnames.append(npvsubname)

    filename_dict = {"bare": baresubnames,
                    "pv": pvsubnames,
                    "npv": npvsubnames}

    # now map these
    # and collect the results
    batch_submit = create_median_image.map(grid, epsgcodes, all_items, baresubnames,
                     pvsubnames, npvsubnames)
    # we need to wait for each of these tasks
    logger.info("waiting for medians to all finish")
    final_states = [future.wait() for future in batch_submit]
    logger.info(f"first element was {final_states[0]}")


    # you have to run the save_fcover separately
    for band, basename in zip(("bare", "npv", "pv"), (barename, pvname, npvname)):
        vrtname = f"{basename}.vrt"
        inputnames = " ".join(filename_dict[band])
        cmd = f"gdalbuildvrt {vrtname} {inputnames}"
        logger.info(f"running command {cmd}")
        shell_run_command(f"gdalbuildvrt {vrtname} {inputnames}")

    # now convert to cog and save
    # this is computationally expensive, so needs to run sequentially
    baresave = save_fcover( f"{barename}.vrt", "esp-rss-imagerepo", aws_key.get(), aws_secret.get())
    pvsave = save_fcover( f"{pvname}.vrt", "esp-rss-imagerepo", aws_key.get(), aws_secret.get(),
                         wait_for=[baresave])
    npvsave = save_fcover( f"{npvname}.vrt", "esp-rss-imagerepo", aws_key.get(), aws_secret.get(),
                        wait_for=[baresave, pvsave])


    # upate the catalog
    # all the images will have the same characteristics
    # so we just need one (bare) as the templated
    vrtname = f"{barename}.vrt"
    stacid = f"{qvf.locationid(vrtname)}_{qvf.when(vrtname)}_asdi"
    # what to do if the item exists
    # we delete
    delete_item(stacid, stac_user.get(), stac_password.get())
    update_stac_median(vrtname, stac_user.get(), stac_password.get())
    logger.info("all tasks done")

