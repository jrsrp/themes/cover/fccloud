"""
A flow which takes a tile, and a start month and end month
gets all the images and processes them.
"""

import json
import shapely
from prefect import flow
from prefect.deployments import run_deployment
from pystac_client import Client
from rsc.utils import metadb

from utils import yearmon_to_daterange

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def aoi_to_poly(aoistr):
    """Convert goejson to shapely polygon.

    Check, this might not work for multipolygon
    Args:
        aoi (json): geojson string for your aoi

    Returns:
        Polygon: a shapely.geometry.Polygon
    """
    aoi = json.loads(aoistr)
    try:
        aoipoly = aoi['features'][0]['geometry']
        coords = aoipoly['coordinates'][0]
        aoi_as_shapely_polygon = shapely.geometry.Polygon(coords)
    except KeyError:
        aoi_as_shapely_polygon = shapely.geometry.Polygon(
            aoi['coordinates'][0]
        )
    return aoi_as_shapely_polygon


def get_items_in_aoi(geom, daterange):
    awscatalog = Client.open('https://earth-search.aws.element84.com/v1/')

    awssearch = awscatalog.search(
        max_items=2000,
        collections=['sentinel-2-l2a'],
        intersects=geom,
        datetime=daterange,
    )

    s2items = awssearch.item_collection()
    s2ids = set([item.id for item in s2items])
    return s2ids


def get_aoi(tile):
    with metadb.connect() as conn:
        with conn.cursor() as cursor:
            cursor.execute(
                'select st_asgeojson(geom) from sentinel2_tiles where scene = %s',
                (tile.lower(),),
            )
            res = cursor.fetchall()
    aoi = res[0][0]
    return aoi


def filter_ids_by_tile(ids, tile):
    """Filter ids to just those that match the tile

    Args:
        ids (list): list of ids
        tile (str): tile to match (like t54kua)

    Returns:
        list: list of ids, a subset of ids
    """
    tiles = [id for id in ids if 't' + id.split('_')[1].lower() == tile]
    return tiles


@flow(name='season run', retries=0)
def tile_flow(tile, startmonth, endmonth, n=6):
    startdate, _ = yearmon_to_daterange(startmonth)
    _, enddate = yearmon_to_daterange(endmonth)
    aoi = get_aoi(tile)
    aoi_as_shapely_polygon = aoi_to_poly(aoi)
    theseids = get_items_in_aoi(aoi_as_shapely_polygon, (startdate, enddate))
    subids = filter_ids_by_tile(theseids, tile)
    for subitems in chunks(list(subids), n):
        myparams = {'satids': subitems}
        run_deployment('fcv3_single_date/fcv3', parameters=myparams, timeout=0)
