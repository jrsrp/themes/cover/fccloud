import rsc.qvf as qvf
from prefect import flow
from prefect.deployments import run_deployment

@flow(name='qld median season run')
def qld_median_flow(startmonth, endmonth):
    for tile in qvf.QLD_SENTINEL_TILES:
        myparams = {
            'startmonth': startmonth,
            'endmonth': endmonth,
            'tile': tile,
        }
        run_deployment('do median/median_tile', parameters=myparams, timeout=0)

@flow(name='qld sub median season run', description="a subset of the run for testing purposes")
def qld_sub_median_flow(startmonth, endmonth):
    for tile in qvf.QLD_SENTINEL_TILES[0:100]:
        myparams = {
            'startmonth': startmonth,
            'endmonth': endmonth,
            'tile': tile,
        }
        run_deployment('do median/median_tile', parameters=myparams, timeout=0)
