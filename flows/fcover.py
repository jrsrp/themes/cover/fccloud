#!/bin/env python3
"""
Calculate fractionalcover3 given an s2l2a id
"""

from contextlib import ExitStack
import rasterio as rio
from rasterio.enums import Resampling
from rasterio.vrt import WarpedVRT
import numpy as np
from pystac_client import Client
import tempfile
import requests
from fractionalcover3 import data, unmix_fractional_cover


from jrsrp_sen2brdf import sen2l2a_brdf

from collections import namedtuple

BandNames = namedtuple("Bands", ["B02", "B03", "B04", "B08", "B11", "B12"])


# how we map bands
bandnames = BandNames(
    B02="blue", B03="green", B04="red", B08="nir", B11="swir16", B12="swir22"
)


def fudgeSen2toLandsat_sfcRef(refimage, outputSensor, nullVal, inputSensor="A"):
    """
    Convert Sentinel-2 surface reflectance values to look like Landsat
    surface reflectance. Can fudge to either Landsat-7 ETM+ or Landsat-8 OLI,
    depending on the value of outputSensor (either 7 or 8). There is no
    coincident data between Sentinel-2 and Landsat-5, for obvious reasons.

    The inputSensor defaults to 'A', for S2A, but after S2B is launched, we will
    check how comparable they are, and might need to fit further fudges, or something.

    This is an empirical fudge, derived from matching pairs of coincident cloud-free
    imagery from the two sensor, across large parts of Australia (Qld,NSW,Vic,NT), up to
    2017-01-31.

    The input should be Sentinel-2 surface reflectance, in the range [0,1], i.e. it has been
    unscaled back to physical units. It is assumed that the two pixel sizes have already
    been combined, and only the corresponding bands selected. The input array can have
    somewhat arbitrary shape, but the first dimension must be the band, and must be of
    length 6, i.e. the shape of the array is (6, ....).

    The shape is preserved on output. The output values are also reflectance values, but
    fudged to be more like Landsat values.

    Null values in the input are copied across to the output.

    """
    nBands = 6

    if refimage.shape[0] != nBands:
        raise ValueError(
            "Input array must have {} bands, this has {}".format(
                nBands, refimage.shape[0]
            )
        )

    # Univariate model coefficients
    if outputSensor == 7 and inputSensor == "A":
        c0 = [-0.0022, 0.0031, 0.0064, 0.012, 0.0079, -0.0042]
        c1 = [0.9551, 1.0582, 0.9871, 1.0187, 0.9528, 0.9688]
    elif outputSensor == 8 and inputSensor == "A":
        c0 = [-0.0012, 0.0013, 0.0027, 0.0147, 0.0025, -0.0011]
        c1 = [0.963, 1.0473, 0.9895, 1.0129, 0.9626, 0.9392]
    else:
        msg = "Input sensor '{}' and output sensor '{}' is not a known combination".format(
            inputSensor, outputSensor
        )
        raise ValueError(msg)

    outRef = []
    for i in range(nBands):
        fudged = c0[i] + c1[i] * refimage[i]
        fudged = fudged.clip(0, 1)
        outRef.append(fudged)

    outImage = np.array(outRef)

    nullMask = refimage == nullVal
    outImage[nullMask] = nullVal
    return outImage


def get_angle_info(href):
    """
    Get the angle info from an xml file from a url.
    """

    with tempfile.NamedTemporaryFile(mode="w+b", delete=True) as tmpfh:
        r = requests.get(href, allow_redirects=True)
        tmpfh.write(r.content)
        tmpfh.flush()
        angleInfo = sen2l2a_brdf.AngleInfo(xmlfilename=tmpfh.name)
    return angleInfo


def brdf_corr(angleinfo, topy, leftx, data, res, band_name, sunZenStd=45.0):
    """
    Given an angleinfo object and data, use the
    reasnmplendx and determine gamma for appropriate
    band.
    """
    resample_tile = angleinfo.calcResampleNdx(
        res, topy, leftx, data.shape[0], data.shape[1]
    )
    gamma = angleinfo.calcStdAdjustFactor(sunZenStd, band_name)
    gammaImg = gamma[resample_tile]
    outdata = data * gammaImg
    return outdata


def compute_fcover(satid, dest_bare, dest_green, dest_dry, catalogue):
    """
    Calculate fractional cover by using the data pointed to in
    the 10m and 20m href bands.
    Outputs go to three files.
    satid like S2B_54KXA_20230121_0_L2A
    dest filenames

    """

    catalog = Client.open(catalogue)
    search = catalog.search(collections=["sentinel-2-l2a"], ids=[satid])
    items = search.get_all_items()
    selected_item = items[0]

    # we have two sets of band names, because one is for the assets
    # the other is for the records in the metadata
    # so we map them

    bands_10m = [bandnames.B02, bandnames.B03, bandnames.B04, bandnames.B08]
    bands_20m = [bandnames.B11, bandnames.B12]

    href_10m = [selected_item.assets[band].href for band in bands_10m]
    href_20m = [selected_item.assets[band].href for band in bands_20m]

    # get the angle info
    xmlref = selected_item.assets["granule_metadata"].href
    angleInfo = get_angle_info(xmlref)

    # if we're using the local cache, we rewrite the hrefs,
    # and create the gdalrc file

    with ExitStack() as stack:
        rio_10msrcs = [stack.enter_context(rio.open(href)) for href in href_10m]
        rio_20msrcs = [stack.enter_context(rio.open(href)) for href in href_20m]
        # use the first 10m as a template for the warping
        template_src = rio_10msrcs[0]
        vrt_options = {
            "resampling": Resampling.cubic,
            "crs": template_src.profile["crs"],
            "transform": template_src.profile["transform"],
            "height": template_src.profile["height"],
            "width": template_src.profile["width"],
        }
        vrt_srcs = [
            stack.enter_context(WarpedVRT(origsrc, **vrt_options))
            for origsrc in rio_20msrcs
        ]
        # and  our output file
        profile = template_src.profile
        profile["dtype"] = "uint8"
        profile["nodata"] = 255
        profile["count"] = 1
        profile["driver"] = "GTiff"

        bare_dest_fh = stack.enter_context(rio.open(dest_bare, "w", **profile))
        green_dest_fh = stack.enter_context(rio.open(dest_green, "w", **profile))
        dry_dest_fh = stack.enter_context(rio.open(dest_dry, "w", **profile))

        # we do windowed reading
        for ji, window in template_src.block_windows(1):
            ref_list = []

            for bandname, src in zip(bandnames._fields, rio_10msrcs + vrt_srcs):
                this_ref = src.read(1, window=window).astype("float")
                # apply our brdf correction
                (leftx, topy) = rio.transform.xy(
                    template_src.transform, window.row_off, window.col_off, offset="ul"
                )
                pixsize = 10  # we've made everything 10m
                refcorr = brdf_corr(angleInfo, topy, leftx, this_ref, pixsize, bandname)
                # does this actually work?
                refcorr[this_ref == src.profile["nodata"]] = np.nan
                ref_list.append(refcorr)
            # convert to an array
            ref = np.stack(ref_list)
            # this will now be (6 x nrow x ncol)
            refscaled = np.where(
                ref == template_src.profile["nodata"], -1, ref * 0.0001
            )
            # fudget to l7
            refl7 = fudgeSen2toLandsat_sfcRef(refscaled, 7, -1)
            # do our prediction
            tffractions = unmix_fractional_cover(refl7, fc_model=data.get_model())
            fcLayers = np.clip(tffractions, 0, 2)
            fcLayerSum = fcLayers.sum(axis=0) + np.finfo("float32").eps
            fractions = fcLayers / fcLayerSum
            fractions[fractions > 1.0] = 1.0
            # scale and write out
            fractions_int = np.where(
                np.isnan(fractions), 255, (fractions * 100).astype("uint8")
            )
            nbands, nrow, ncol = fractions_int.shape
            # note that tffractions will give a value even when input is nan
            # so we set these to 255
            fractions_int = np.where(np.isnan(refl7[0:3]), 255, fractions_int)
            bare_dest_fh.write(fractions_int[0].reshape((1, nrow, ncol)), window=window)
            green_dest_fh.write(
                fractions_int[1].reshape((1, nrow, ncol)), window=window
            )
            dry_dest_fh.write(fractions_int[2].reshape((1, nrow, ncol)), window=window)
    return 0

