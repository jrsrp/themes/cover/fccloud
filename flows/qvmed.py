from contextlib import ExitStack
import affine
from pathlib import Path

# for S3 Access
from rasterio.session import AWSSession
import rsc.qvf as qvf
from rasterio.enums import Resampling
from rasterio.vrt import WarpedVRT
from rasterio.crs import CRS
import numpy as np
from prefect.blocks.system import Secret
from prefect import task
from rsc.stats import vectormedian
import rasterio as rio
from shapely.geometry import Polygon
from shapely.prepared import prep
from prefect import get_run_logger
aws_secret = Secret.load("imgreposecret")
aws_key = Secret.load("imgrepokey")

STAC_SERVER= "http://espb4stac.dnr.qld.gov.au/api/v1/pgstac/"


# to do
# * mask with the footprint
# * use awssession properly
# * check that the epsg code works in general
# * do I need to do a date season month thing


# from generateSen2Aw1.py
def snap_value(value):
    """
    Snap the given value to the nearest multiple of 10
    """
    return round(value / 10) * 10


@task(name="create output filename")
def get_outfile_name(href, whenstr, destdir=".", outstage="aj9"):
    """
    Given an href, a when string, create a filename

    """
    basename = qvf.changestage(Path(href).name, outstage)
    basename = qvf.changedate(basename, whenstr)
    outfile = Path(destdir) / Path(basename)
    return outfile


def grid_bounds(geom, delta, extra=10):
    """
    Split it up, evenly, but last one might be a bit bigger
    """
    minx, miny, maxx, maxy = geom.bounds
    minx = snap_value(minx) - extra
    maxx = snap_value(maxx) + extra
    miny = snap_value(miny) - extra
    maxy = snap_value(maxy) + extra

    nx = int((maxx - minx) / delta) + 1
    ny = int((maxy - miny) / delta) + 1
    gx, gy = minx + np.arange(nx) * delta, miny + np.arange(ny) * delta
    gx[-1] = maxx
    gy[-1] = maxy
    grid = []
    for i in range(len(gx) - 1):
        for j in range(len(gy) - 1):
            poly_ij = Polygon(
                [
                    [gx[i], gy[j]],
                    [gx[i], gy[j + 1]],
                    [gx[i + 1], gy[j + 1]],
                    [gx[i + 1], gy[j]],
                ]
            )
            grid.append(poly_ij)
    return grid


def partition(geom, delta):
    prepared_geom = prep(geom)
    grid = list(filter(prepared_geom.intersects, grid_bounds(geom, delta)))
    return grid





@task(name="vectormedian", retries=0)
def create_median_image(geom, epsgcode, items, bareoutfile, pvoutfile, npvoutfile):
    """
    given a tile, a list of items, and a band, create
    a single band median image.

    I need to make this a stack with t x 3 x m x n
    t is number of dates
    geom should already be in the output crs (eg 32755)
    """
    logger = get_run_logger()
    # so now, lets see if we can process using the median
    # get the bare, pv, npv refs
    bare_refs = []
    pv_refs = []
    npv_refs = []
    for item in items:
        # all three need to be there
        if "bare" in item.assets and "pv" in item.assets and "npv" in item.assets:
            bare_refs.append(item.assets["bare"].href)
            pv_refs.append(item.assets["pv"].href)
            npv_refs.append(item.assets["npv"].href)
    # you should check the stage code here too

    awssession = AWSSession(
        aws_access_key_id=aws_key.get(), aws_secret_access_key=aws_secret.get()
    )

    # we want our aoi to be the same. We just need it in the correct
    # coordinates

    dst_bounds = geom.bounds
    (xmin, ymin, xmax, ymax) = dst_bounds
    extra = 10.0
    xmin = snap_value(xmin) - extra
    xmax = snap_value(xmax) + extra
    ymin = snap_value(ymin) - extra
    ymax = snap_value(ymax) + extra

    # we use an image for the profile
    with rio.Env(awssession):
        with rio.open(bare_refs[0]) as fh:
            base_profile = fh.profile

    dst_transform = affine.Affine(10, 0.0, xmin, 0.0, -10, ymin)

    base_profile["width"] = (xmax - xmin) / 10
    base_profile["height"] = (ymax - ymin) / 10
    base_profile["crs"] = CRS.from_epsg(epsgcode)
    base_profile["transform"] = dst_transform

    # we want to warp with set bounds
    # to get the bounds, we'll use wkt and shapely

    # we know the bounds
    # we know the crs
    # so should be right to get the profile

    vrt_options = {
        "resampling": Resampling.cubic,
        "crs": CRS.from_epsg(epsgcode),
        "transform": dst_transform,
        "height": (ymax - ymin) / 10,
        "width": (xmax - xmin) / 10,
    }

    with rio.open(bareoutfile, "w", **base_profile) as bare_dst_dataset, rio.open(
        pvoutfile, "w", **base_profile
    ) as pv_dst_dataset, rio.open(npvoutfile, "w", **base_profile) as npv_dst_dataset:
        with rio.Env(awssession):
            with ExitStack() as stack:
                baresrc = [stack.enter_context(rio.open(href)) for href in bare_refs]
                barevrts = [WarpedVRT(src, **vrt_options) for src in baresrc]

                pvsrc = [stack.enter_context(rio.open(href)) for href in pv_refs]
                pvvrts = [WarpedVRT(src, **vrt_options) for src in pvsrc]

                npvsrc = [stack.enter_context(rio.open(href)) for href in npv_refs]
                npvvrts = [WarpedVRT(src, **vrt_options) for src in npvsrc]

                # now go through and get the data for each image
                # since I've blocked at the input level, I don't have to
                # do this by block
                # these should all be the same size
                # need to do so we end up with t x 3 x m x n
                # where t is the number of images
                # a band for each of (bare, pv, npv)
                # m and n rows and cols
                imgdata = []
                for bvrt, pvrt, npvrt in zip(barevrts, pvvrts, npvvrts):
                    this_data = [vrt.read(1) for vrt in (bvrt, pvrt, npvrt)]
                    this_stack = np.array(this_data)
                    imgdata.append(this_stack)

                imgstackDN = np.array(imgdata)
                # so this will have shape (ndates, 3, nrow, ncol)
                # but we want (ndates, 3, nrow, ncol)
                (medianNdx, imgCount) = vectormedian.vectormedian(
                    imgstackDN, nullVal=base_profile["nodata"]
                )
                medianImg = vectormedian.selectmedianimage(
                    imgstackDN, medianNdx, nullVal=base_profile["nodata"]
                )
                if medianImg.shape[0] != 3:
                    logger.error("WARNING: wrong shape probs an error")
                # we write each out as separate images
                # each needs to be (1, nrow, ncol)
                # I think this fails sometime
                bare_dst_dataset.write(medianImg[0], indexes=1)
                pv_dst_dataset.write(medianImg[1], indexes=1)
                npv_dst_dataset.write(medianImg[2], indexes=1)

